// [IMPORTS]
let jwt = require('jsonwebtoken');
let project = "ECommerceAPI";

//TOKEN CREATION
module.exports.createAccessToken = (user) => {
	let data = {
		id:user._id,
		email:user.email,
		isAdmin:user.isAdmin
	};

	return jwt.sign(data, project, {})
};

//USER VERIFICATION
module.exports.verify = (req, res, next) => {

	let token = req.headers.authorization

	if (typeof token === "undefined"){

		res.send({auth: "No Token found"})
	} else {
		token = token.slice(7, token.length);
		jwt.verify(token, project, function (error, decodedToken){

			if(error){
				res.send({auth:"Failed", message: error.message});
			} else {

				req.user = decodedToken

				next();
			}
		})
	}
};

// ADMIN VERIFICATION

module.exports.verifyAdmin = (req, res, next) => {
	if(req.user.isAdmin){
		next();
	}else {
		res.send ({auth: "Forbidden", message:"For admin access only"})
	}
}