//Dependencies and Modules
let Product = require ('../models/Product');


//FUNCTIONALITY [CREATE]
module.exports.createProduct = (data) => {
	let pName = data.productName;
	let pDesc = data.description;
	let pPrice = data.price;
	let pStock = data.stock

	let newProduct = new Product ({
		productName:pName,
		description:pDesc,
		price:pPrice,
		stock:pStock

	})

	return newProduct.save().then((saveProduct, err) => {
		if (saveProduct){
			return saveProduct;
		} else {
			return 'failed to save new product'
		}
	})
}
//[RETRIEVE PRODUCT]

//retrieve/archive all products
module.exports.getAllProduct = () => {
	return Product.find({}).then(result => {
		return result;
	})
};

//retrieve 1 product using id
module.exports.getProduct = (id) => {
	return Product.findById(id).then(result => {
		return result;
	})
};

//retrieve all active products
module.exports.getActiveProducts =() =>{
	return Product.find({isActive:true}).then(result => {
		return result;
	})
};



module.exports.listAllProduct = () => {
	return Product.find
}

//[UPDATE Product]
//updating the product info
module.exports.updateProduct = (id, details) => {

	// let pName = details.productName;
	// let pDesc = details.description;
	let pCost = details.price


	let updatedProduct = ({
		// productName:pName,
		// description:pDesc,
		price:pCost,
	
	})


	return Product.findByIdAndUpdate(id, updatedProduct).then((productUpdated, error) => {
		if (error){
			return 'Failed to update the product'
		} else {
			return "Successfully updated the product" 
		}
	})
};

//archive product (to make inactive)
module.exports.productArchive = (id) => {
	let inactive ={
		isActive:false
	}
	return Product.findByIdAndUpdate(id, inactive).then((archived, err) => {
		if (archived){
			return `Product ${id} is archived successfully`
		} else {
			return 'Failed to archived product'
		}
	})
};
//reactivate product
module.exports.productActivate = (id) => {
	let activate = {
		isActive: true
	}
	return Product.findByIdAndUpdate(id, activate).then((reactivate, err) => {
		if (reactivate){
			return `Product ${id} successfully reactivated`
		} else {
			return ' failed to activate product'
		}
	})
}
// [DELETE Product]
//deleting product by ID

module.exports.deleteProduct = (id) => {
	return Product.findByIdAndRemove(id).then((deletedProd, err)=>{
		if (err){
			return 'Product not deleted'
		} else {
			return 'Product successfully deleted'
		}
	})
}