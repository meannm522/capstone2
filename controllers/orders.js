//Dependencies

let Order = require ('../models/Order');
let Product = require('../models/Product')
let User = require ('../models/User')
let auth = require ('../auth')

let {verify, verifyAdmin} = auth
let exp = require("express");


//Functionality [CREATE]
//get the details of the user who will order//only useraccess can order
// userId, 


/*
1. only verified user can post order
2. once verified, order will be created, user need to input product id and qty
3. need to look for the product using the product id to get
product price 
4. purchases subdocument will be push  to order includes
userID and the product id 

*/ 

module.exports.createOrder = async (req, res) => {
	
	let buyer = req.user.email
	let prodId = req.body.productId
	let qty = req.body.quantity
	let isAdmin = req.user.isAdmin
	// console.log (req.user.isAdmin)
	// console.log(req.user.email)
	// console.log(req.body.quantity)
	
		if (isAdmin){
			res.send ('Incorrect access, use a User account')
	} 

	let orderProduct = await Product.findById(prodId).then(result => result)
		console.log(orderProduct.price)
	
		let newOrder = new Order ({
			email:buyer,
			purchases:[{
				productId:prodId,
				quantity:qty,
			}],

			totalAmount: (orderProduct.price * qty)
				
		});
	
		console.log(newOrder)
		return newOrder.save().then((saveOrder,err)=>{
			if(saveOrder){
				return res.send (saveOrder)
			} else {
				return 'Failed to save order'
			}
		})
}

//Get one order
module.exports.showOrder = (id) => {

	return Order.findById(id).then (result => {
		return result
	})
};


//update order
// module.exports.updateOrder = (id, details) => {
	
// 	let prodId = details.productId
// 	let qty = details.quantity
	
	
// 	let updatedOrder = [{
// 		productId: prodId,
// 		quantity:qty
// 	}]

// 	return Order.findByIdAndUpdate(id, updatedOrder).then ((update, err) => {
// 		console.log(id)
// 		// console.log(updatedOrder)
// 		// console.log(update)
		
// 		if (err){
			
// 			return ("Failed to update order")
// 		} else {
// 			return   ('Successfully updated order')
// 		}
// 	});
// };
// module.exports.updateOrder = (id, details) => {
// 	let prodId = details.productId
// 	let qty = details.quantity

// 	let updatedOrder = {
// 		productId: prodId,
// 		quantity:qty
// 	}
// 	return Order.findByIdAndUpdate(id, updatedOrder).then((saveUpdate,err) => {
// 		// console.log(updatedOrder)
		
// 		if (saveUpdate){
			
// 			return (saveUpdate)
// 		} else {
// 			return  ('Failed to update order')
// 		}
// 	});
// };

//Get all order 
module.exports.showAllOrder = (req, res) => {
	return Order.find({}).then (result => {
		return res.send (result)
	})
};
	
