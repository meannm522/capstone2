//DEPENDENCIES
	let User = require('../models/User');
	let dotenv = require ("dotenv");
	let bcrypt = require ("bcrypt");
	let auth = require ("../auth");
//ENV setup
	dotenv.config();
	let salt = parseInt(process.env.SALT)
//[CREATE]
module.exports.registerAccount = (data) => {

	let userEmail = data.email;
	let passW = data.password;

	let newUser= new User ({
		email: userEmail,
		password: bcrypt.hashSync(passW,salt)
	});

	return newUser.save().then ((user, err) =>{
		if (user){
			return user;
		} else {
			return 'Failed to register a new user account'
		}
	})
};

// USER LOG IN TO GET TOKEN

module.exports.logIn =(req, res) => {
	User.findOne({email:req.body.email}).then(userFound =>{
		if (userFound === null){
			return res.send("Account not Found")
		} else {
			let isPasswordMatch = bcrypt.compareSync(req.body.password, userFound.password)

			if (isPasswordMatch){
				res.send({accessToken: auth.createAccessToken(userFound)})
			} else {
				return res.send('Password is Incorrect')
			}
		}
	}).catch(err => res.send(err))
};
//[RETRIEVE]


//GET ALL USER
module.exports.getAllUser = () =>{
	return User.find({}).then(result =>{
		return result
	})
}


//[UPDATE]
//updating user password

module.exports.updateUser = (id, details) =>	 {
	let password = details.password
	let email = details.email

	let updatedUser = {
		
		password:bcrypt.hashSync(password,salt)
	}

	return User.findByIdAndUpdate(id, updatedUser).then((userUpdated, err) => {
		if (err){
			return "Failed to update user password"
		} else {
			return `Your new password is updated ${userUpdated}`
		}
	})
}; 

//update user to isAdmin=true

module.exports.updateUserToAdmin = (id) =>{
	let setToAdmin = {
		isAdmin:true
	}
	return User.findByIdAndUpdate(id, setToAdmin).then((nowAdmin, err) => {
		if(err){
			return "failed to update user to admin"

		} else {
			return "Successfully updated to admin"
		}
	})

}
//[DELETE]
