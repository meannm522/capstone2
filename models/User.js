//DEPENDECIES 
let mongoose = require ("mongoose");

//SCHEMA

let userSchema = new mongoose.Schema ({
	email:{
		type: String,
		required: [ true, 'Email address is required']
	},
	password:{
		type: String,
		required: [true, 'Password is required']
	},
	isAdmin:{
		type: Boolean,
		default: false
	}
	
})

let User = mongoose.model('USER', userSchema)
	module.exports = User; 