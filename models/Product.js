//DEPENDECIES 
let mongoose = require ("mongoose");

//SCHEMA

let productSchema = new mongoose.Schema ({
	productName:{
		type: String,
		required: [true, 'Product name is required']
	},
	description:{
		type: String,
		required: [true, 'Product description is needed']
	},
	price:{
		type: Number,
		required: [ true, 'Price is required']
	},
	stock:{
		type: Number,
		required: [true, 'stock is required']
	},
	isActive:{
		type: Boolean,
		default: true
	},
	// NumberOfStock:{
	// 	type: Number,
	// 	required: true
	// },
	createdOn:{
		type: Date,
		default: new Date()
	}
	
})

let Product = mongoose.model("Product", productSchema)

	module.exports = Product;