//DEPENDECIES 
let mongoose = require ("mongoose");


let orderSchema = new mongoose.Schema ({
	
	email:{
		type: String,
		required: true
	},
	
	purchases:[
		{
			
			productId:{
				type: String,
				required: true
		},
			quantity:{
				type: Number,
				required: true
		
		}

		}

	],
		
	totalAmount:{
			type: Number,
			required: true
		},

	purchasedOn:{
		type: Date,
		default: new Date()
	}
	
})
// [SECTION] MODEL
	
const Order = mongoose.model("Order", orderSchema)

	module.exports = Order;