//DEPENDENCIES
let express = require("express");
let mongoose = require("mongoose");
let dotenv = require ("dotenv");
const cors = require('cors');
// routing component from the users collection\
let productRoutes = require('./routes/products');
let userRoutes = require('./routes/users');
let orderRoutes = require('./routes/orders')

// //SERVER SETUP
let app = express();
dotenv.config();
app.use (express.json());
let connectString = process.env.CONNECTION_STRING;
let port = process.env.PORT;

//application routes
app.use('/users',userRoutes);
app.use('/products', productRoutes);
app.use('/orders', orderRoutes);
app.use(cors());

app.use((req, res, next) =>{
	const origin = req.headers.origin;
	if(allowedOrigins.includes(origin)) {
		res.setHeader('Access-Control-Allow-Credentials', true);
	}
	return next()
});

//DATABASE CONNECT
mongoose.connect(connectString)

let connectStat= mongoose.connection;
connectStat.on ('open', () => console.log ('Connected to Database'));

//GATEWAY RESPONSE
app.get('/', (req,res) => {
	res.send (`Welcome to Max's eStore`)
})
app.listen( port, () => console.log(`Server currently running on port ${port}`))