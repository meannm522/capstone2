//DEPENDENCIES
	let exp = require("express");
	let controller = require('./../controllers/orders.js');
	let user = require('./../controllers/users.js')
	let auth = require ('../auth')
	//destructure
	let {verify, verifyAdmin} = auth
//Routing
	let route = exp.Router();

// [POST]

route.post('/createOrder', verify, controller.createOrder)



//show  order by id

route.get('/all', verify, verifyAdmin, controller.showAllOrder)
// searching orders by orderId


route.get('/:id', verify,  (req, res) =>{
	let orderId = req.params.id
	controller.showOrder(orderId).then(result => {
		res.send(result)
	})
});



//edit order
// route.put('/:id', verify, (req, res) => {
	
// 	let id = req.params.id;
// 	let details = req.body


// 	let prodId = details.productId
// 	let qty = details.quantity
		
// 		if 	(prodId !=='' && qty !=='') {
		
// 		controller.updateOrder(id, details).then (result => {
// 			res.send(result);
// 		})

// 	} else {
// 		res.send('Incomplete input, please recheck')
// 	}

// });

// route.get()
// route.post('/order', (req, res) => {
		
// 		let orderInfo = req.body;
// 		controller.newOrder(orderInfo).then(outcome =>{
// 			res.send (outcome);
// 		});
// 	});

//EXPORT
module.exports = route;
