//DEPENDENCIES
	let exp = require("express");
	let controller = require('./../controllers/users.js');
	let auth = require ('../auth')
	const User = require('../models/User');	
	//destructure
	let {verify, verifyAdmin} = auth
//Routing
	let route = exp.Router();

//ROUTES [POST]
	route.post('/register', (req, res) => {
		
		let userInfo = req.body;
	User.findOne({email:req.body.email}, (err, result) =>{
		
		if(result!= null && result.email == req.body.email){
			return res.send("Email already used")
		} else {
			controller.registerAccount(userInfo).then(outcome =>{
			res.send (`Succesfully created an account ${req.body.email}`);
		});
		}
	})
	});

//LOG IN 
	route.post('/login', controller.logIn)


// ROUTES [GET]
// RETRIEVE ALL USER
	route.get('/allUser',verify, verifyAdmin, (req, res) => {
		controller.getAllUser().then(result => {
			res.send (result);
	})
});

//[PUT]
	route.put('/:id', verify, (req, res) =>{
		let id = req.params.id;
		let details = req.body;

		let newPassword = details.password;

		if (newPassword !== ''){
			controller.updateUser(id, details).then(outcome	 => {
				res.send (outcome)
			})
		} else {
			res.send ("Please provide an updated password")
		}
	})

	route.put('/:id/admin', verify, verifyAdmin, (req, res) => {
		let userID = req.params.id

		controller.updateUserToAdmin(userID).then(result => {
			res.send(result)
		})
	})

//Route system
	module.exports = route;