//Dependencies and Modules
let exp = require ("express");
let controller = require ('./../controllers/products.js');
let auth = require ('../auth');
	let {verify, verifyAdmin} = auth

//Routing component

let route = exp.Router()

//[POST] routes
//create a new product
route.post('/create', verify, verifyAdmin, (req, res) => {

	let productInfo = req.body;
	controller.createProduct(productInfo).then (outcome => {
		res.send (outcome);
	})
});

//[GET]
//display all products
route.get('/displayAll', verify, verifyAdmin, (req,res) => {
	controller.getAllProduct().then(result => {
		res.send(result);
	})
});
//display one roduct by ID

route.get('/:id', verify, (req, res) => {
	let productId = req.params.id;
	controller.getProduct(productId).then(outcome => {
		res.send(outcome);
	})
});
//display active product
route.get('/', verify, (req, res) => {
	controller.getActiveProducts().then(result => {
		res.send(result);
	})
});



//[PUT]
//updating product info
route.put('/:id', verify, verifyAdmin, (req, res) =>{
	let id = req.params.id;
	let details = req.body

	// let pName = details.productName;
	// let pDesc = details.description;
	let pCost = details.price

	if 	(pCost !=='') {
		controller.updateProduct(id,details).then(result =>{
			res.send(result);
		})
	} else {
		res.send('Incomplete input, please recheck')
	}
});
//Archive product marked inactive
route.put('/:id/inactive', verify, verifyAdmin, (req, res) => {
	let id = req.params.id;
	// if (id === null){
	// 	res.send ('Please input the product id to be archived')
	// }
	controller.productArchive(id).then(result => {
		res.send (result)
	})

});
//Activate product
route.put('/:id/activate', verify, verifyAdmin,  (req,res) => {
	let id = req.params.id

	controller.productActivate(id).then(result => {
		res.send(result)
	})
})

//[DELETE]
//deleting product
route.delete('/:id', verify, verifyAdmin, (req, res)=>{
	let id =req.params.id;
	controller.deleteProduct(id).then(result => {
		res.send(result)
	})
});

//[EXPORT ROUTE]

module.exports = route;